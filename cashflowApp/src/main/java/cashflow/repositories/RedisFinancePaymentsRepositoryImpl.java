package cashflow.repositories;

import cashflow.exception.LoanFacilityNotFoundException;
import cashflow.model.Payment;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
@ConditionalOnProperty(value = "storage.cashflow", havingValue = "redisStorage")
public class RedisFinancePaymentsRepositoryImpl implements FinancePaymentsRepository {
    private static final Logger logger = LoggerFactory.getLogger(RedisFinancePaymentsRepositoryImpl.class);
    Gson gson = new Gson();
    @Resource(name = "financeTemplate")          // 'redisTemplate' is defined as a Bean in Configuration.java
    private RedisTemplate<String, Payment> redisTemplate;

    private final String hashReferencePayments = "Payments";
    private HashOperations<String, String, Payment> financeDB;

    @Override
    public void initialize() {
        financeDB = redisTemplate.opsForHash();
    }

    @Override
    public Payment getFinancePayments(String loanId) throws LoanFacilityNotFoundException {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);

        var d = financeDB.get(hashReferencePayments, loanId);
        Payment payment =  objectMapper.convertValue(d,Payment.class);
        if (payment == null)
           throw new LoanFacilityNotFoundException("Not found id"+ loanId);
        return payment;
    }

    @Override
    public boolean setFinancePayments(Payment financePayments) {

        try {
            financeDB.put(hashReferencePayments, financePayments.getId(),financePayments);
            return true;
        }
        catch (Exception ex)
        {
            logger.error("RedisFinancePaymentsRepositoryImpl error :"+ ex);
        }
        return false;
    }

    @Override
    public Long countFinancePayments() {

        var i = financeDB.keys(hashReferencePayments);
        return Long.valueOf(i.size());
    }


}

