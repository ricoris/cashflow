package cashflow.repositories;

import cashflow.model.Customer;

import java.util.List;

public interface CustomerRepository {
    void initialize();

    Customer getCustomer(String customerNum);
    List<Customer> getCustomers();

    boolean saveCustomer(Customer customer);


}
