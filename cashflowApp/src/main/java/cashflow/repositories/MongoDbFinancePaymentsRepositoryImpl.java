package cashflow.repositories;

import cashflow.exception.LoanFacilityNotFoundException;
import cashflow.model.Payment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Objects;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
@ConditionalOnProperty(value = "storage.cashflow", havingValue = "mongoStorage")
public class MongoDbFinancePaymentsRepositoryImpl implements FinancePaymentsRepository {
    private static final Logger logger = LoggerFactory.getLogger(MongoDbFinancePaymentsRepositoryImpl.class);

    @Resource(name = "mongoTemplate")          // template is defined as a Bean in Configuration.java
    private MongoOperations mongoOperations;

    @Override
    public void initialize() {

    }

    @Override
    public Payment getFinancePayments(String loanId) throws LoanFacilityNotFoundException {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(loanId));
        var returnData = mongoOperations.findOne(query,Payment.class);
         if (Objects.isNull(returnData))
             throw new LoanFacilityNotFoundException("Not found id"+ loanId);

         return returnData;

    }

    @Override
    public boolean setFinancePayments(Payment financePayments) {
        try {
            Update update = new Update().set("financePayments", financePayments.getFinancePayments());

            mongoOperations.upsert(query(where("id").is(financePayments.getId())), update, "payment");
            return true;
        } catch (Exception ex) {
            logger.error("MongoDbFinancePaymentsRepositoryImpl error :" + ex);
        }
        return false;
    }

    @Override
    public Long countFinancePayments() {
        Query query = new Query();
        long count = mongoOperations.count(query,Payment.class, "payment");
        return  count;
    }


}

