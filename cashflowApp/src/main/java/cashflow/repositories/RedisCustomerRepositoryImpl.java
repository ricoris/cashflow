package cashflow.repositories;

import cashflow.model.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
@ConditionalOnProperty(value = "storage.cashflow", havingValue = "redisStorage")

public class RedisCustomerRepositoryImpl implements CustomerRepository {
    private static final Logger logger = LoggerFactory.getLogger(RedisCustomerRepositoryImpl.class);

    @Resource(name = "customerTemplate")          // 'redisTemplate' is defined as a Bean in Configuration.java
    private RedisTemplate<String, Customer> redisTemplate;
    private final String hashReference = "Cashflow";

    private HashOperations<String, String, Customer> customerDB;

    @Override
    public void initialize() {
        // set up hasoperations - just simpler notation.
        customerDB = redisTemplate.opsForHash();

    }
    @Override
    public boolean saveCustomer(Customer customer)
    {

        // Store it in the repository
        customerDB.put(hashReference, customer.getCustomerNum(), customer);
        return true;

    }

    @Override
    public Customer getCustomer(String customerNum)
    {
        return customerDB.get(hashReference, customerNum);
    }

    @Override
    public List<Customer> getCustomers() {
        return null;
    }


}

