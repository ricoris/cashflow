package cashflow.repositories;

import cashflow.model.Customer;
import cashflow.model.Payment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
@ConditionalOnProperty(value = "storage.cashflow", havingValue = "mongoStorage")
public class MongoDbCustomerRepositoryImpl implements CustomerRepository {
    private static final Logger logger = LoggerFactory.getLogger(MongoDbCustomerRepositoryImpl.class);

    @Resource(name = "mongoTemplate")          // template is defined as a Bean in Configuration.java
    private MongoOperations customerOperations;

    @Override
    public void initialize() {

    }
    @Override
    public boolean saveCustomer(Customer customer)
    {

        // Store it in the repository
        customerOperations.insert(customer);
        return true;

    }

    @Override
    public Customer getCustomer(String customerNum)
    {
        Query query = new Query();
        query.addCriteria(Criteria.where("CustomerNum").is(customerNum));
        return customerOperations.findOne(query, Customer.class);
    }

    @Override
    public List<Customer> getCustomers() {

        return customerOperations.findAll(Customer.class);

    }


}

