package cashflow.repositories;

import cashflow.exception.LoanFacilityNotFoundException;
import cashflow.model.Payment;

public interface FinancePaymentsRepository {
    void initialize();
    Payment getFinancePayments(String LoanNum) throws LoanFacilityNotFoundException;
    boolean setFinancePayments(Payment payment);
    Long countFinancePayments();


}
