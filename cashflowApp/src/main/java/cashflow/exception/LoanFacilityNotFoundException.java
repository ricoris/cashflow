package cashflow.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class LoanFacilityNotFoundException extends RuntimeException {

    public LoanFacilityNotFoundException(String message) {
        super(message);
    }

}
