package cashflow.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Bad request")
public class LoanFacilityException extends RuntimeException {

    public LoanFacilityException(String message) {
        super(message);
    }

}
