package cashflow.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ProductUnirestException extends RuntimeException {
    private static final long serialVersionUID = -3154618962130084535L;

    public ProductUnirestException(String message) {
        super(message);
    }
}
