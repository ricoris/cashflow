package cashflow.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Bad request")
public class CustomerException extends Exception {
    private static final long serialVersionUID = -3154618962130084535L;

    public CustomerException(String message) {
        super(message);
    }
}
