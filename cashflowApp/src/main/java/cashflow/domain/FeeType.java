package cashflow.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FeeType implements Serializable {
    private static final long serialVersionUID = -1L;
    private String id;
    private String  description;
    private String frequency;
    private double amount;
}