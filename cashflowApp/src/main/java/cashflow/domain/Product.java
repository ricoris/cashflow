package cashflow.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Product {
    private @Id String id;
    private String  description;
    private double interestRate;
    private int feeType;
    private int minimumTerm;
    private int maximumTerm;
}