package cashflow;

import cashflow.componet.MessageSender;
import kong.unirest.Unirest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;


import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static kong.unirest.Cache.builder;

/***
 *
 * https://spring.io/guides/gs/messaging-rabbitmq/
 */
@SpringBootApplication
public class CashflowApplication {
	private static final Logger logger = LoggerFactory.getLogger(CashflowApplication.class);
	private static int TIMEOUT = 5000;

	public static void main(String[] args) {

		//Unirest defaults
		Unirest.config().reset();
		Unirest.config()
				.verifySsl(false)
				.followRedirects(true)
				.enableCookieManagement(true)
				.connectTimeout(TIMEOUT)
				.socketTimeout(TIMEOUT)
				.cacheResponses(builder()
						.depth(5) // Depth is the max number of entries cached
						.maxAge(5, TimeUnit.MINUTES))
				.instrumentWith(requestSummary -> {
					long StartMillis = System.currentTimeMillis();
					return (responseSummary, exception) -> logger.info("path: {} status: {} responsetime: {}",
							requestSummary.getRawPath(),
							responseSummary.getStatus(),
							System.currentTimeMillis() - StartMillis);
				});


		SpringApplication springApplication = new SpringApplication(CashflowApplication.class);
		ApplicationContext ctx = springApplication.run(args);
		/*
		 * Document loaded beans for a newbie :) - the list is only sent log if debug: true in application.yml
		 */
		if (Objects.equals(ctx.getEnvironment().getProperty("debug"), "true")) {
			logger.info("********************* BEAN LIST *********************");
			String[] beanNames = ctx.getBeanDefinitionNames();
			Arrays.sort(beanNames);
			for (String beanName : beanNames) {
				logger.info("method=main, Bean: " + beanName);
			}
		}


	}



}
