package cashflow.service;

import com.google.gson.Gson;
import cashflow.model.Customer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SendService {
    private static final Logger logger = LogManager.getLogger(SendService.class);

    @Value("${spring.data.messaging.exchange}")
    String exchange;
    @Value("${spring.data.messaging.routing-key}")
    private String routingkey;

    @Autowired private RabbitTemplate rabbitTemplate;
    private Gson gson;

    public void SendService(RabbitTemplate rabbitTemplate) {

        this.rabbitTemplate = rabbitTemplate;
        gson = new Gson();
    }

    public void CustomerSend(Customer customer, String customerName) {

        logger.info("Message Customer : {}", () -> customer);

        customer.setCustomerName(customerName);

        rabbitTemplate.convertAndSend(exchange, routingkey, customer);


    }
}
