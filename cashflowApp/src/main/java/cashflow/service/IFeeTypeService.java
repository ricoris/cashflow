package cashflow.service;

import cashflow.domain.FeeType;

/**
 * FeeType service
 */
public interface IFeeTypeService {
    /**
     * Get feeType
     * @param id
     * @return FeeType
     */
    FeeType getFeeType(int id);
}
