package cashflow.service;


import cashflow.domain.FeeType;
import cashflow.model.Customer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FeeTypeService implements IFeeTypeService {
    @Value("${service.loan.management-url}")
    public String url;
    @Value("${service.loan.management-port}")
    public String port;

    private String path = "/v1/feetype/";
    private static final Logger logger = LogManager.getLogger(FeeTypeService.class);

    public FeeType getFeeType(int id)
    {
        ObjectMapper objectMapper = new ObjectMapper();

        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Content-Type", "text/html");
        try {
            HttpResponse<String> reply =
                    Unirest
                            .get( "http://"+url+":"+port+path+id)

                            .headers(headers)
                            .asString();
            //logger.info("implementationClass="  + this.getClass().getName()+ " response "+reply.getBody());

            return objectMapper.readValue(reply.getBody(), FeeType.class);

        } catch (UnirestException ex) {
            logger.error(" implementationClass="
                    + this.getClass().getName()
                    + ex);

        } catch (Exception ex) {
            logger.error("implementationClass="
                    + this.getClass().getName()
                    + ex);

        }
        return null;
    }

}
