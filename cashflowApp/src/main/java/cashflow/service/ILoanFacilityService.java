package cashflow.service;

import cashflow.domain.LoanFacility;
import cashflow.exception.LoanFacilityException;
import cashflow.exception.LoanFacilityNotFoundException;

/**
 * Loanfacility
 */
public interface ILoanFacilityService {
    /**
     * get the LoanFacility
     * @param uuid
     * @return LoanFacility
     */
    LoanFacility getLoanFacility(String uuid) throws LoanFacilityNotFoundException, LoanFacilityException;
}
