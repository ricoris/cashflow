package cashflow.service;


import cashflow.domain.Product;
import cashflow.exception.ProductException;
import cashflow.exception.ProductNotFoundException;
import cashflow.exception.ProductUnirestException;
import com.fasterxml.jackson.databind.ObjectMapper;
import kong.unirest.HttpResponse;
import kong.unirest.HttpStatus;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ProductService implements IProductService {
    @Value("${service.loan.management-url}")
    public String url;
    @Value("${service.loan.management-port}")
    public String port;

    private String path = "/v1/product/";
    private static final Logger logger = LogManager.getLogger(ProductService.class);

    public Product getProduct(int id) throws ProductUnirestException, ProductException {
        ObjectMapper objectMapper = new ObjectMapper();

        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Content-Type", "text/html");
        try {
            HttpResponse<String> reply =
                    Unirest.get( "http://"+url+":"+port+path+id)
                           .headers(headers)
                           .asString();
            if (reply.getStatus() == HttpStatus.NOT_FOUND) {
                throw new ProductNotFoundException("Product Not found ProductId:"+id);
            }
            return objectMapper.readValue(reply.getBody(), Product.class);

        } catch (UnirestException ex) {
            logger.error(" implementationClass=" + this.getClass().getName() + ex);
            throw new ProductUnirestException("Unirest exception "+ ex);
        } catch (Exception ex) {
            logger.error("implementationClass="
                    + this.getClass().getName()
                    + ex);
            throw new ProductException("Something went wrong." + ex);
        }
    }

}
