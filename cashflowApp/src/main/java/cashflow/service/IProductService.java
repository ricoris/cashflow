package cashflow.service;

import cashflow.domain.FeeType;
import cashflow.domain.Product;
import cashflow.exception.ProductException;
import cashflow.exception.ProductNotFoundException;
import cashflow.exception.ProductUnirestException;

/**
 * Product service
 */
public interface IProductService {
    /**
     * Getting the product
     * @param id
     * @return Product
     */
    Product getProduct(int id) throws ProductUnirestException, ProductException, ProductNotFoundException;
}
