package cashflow.service;


import cashflow.domain.LoanFacility;
import cashflow.exception.LoanFacilityException;
import cashflow.exception.LoanFacilityNotFoundException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import kong.unirest.HttpResponse;
import kong.unirest.HttpStatus;
import kong.unirest.Unirest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class LoanFacilityService implements ILoanFacilityService {
    @Value("${service.loan.management-url}")
    public String url;
    @Value("${service.loan.management-port}")
    public String port;

    private String path = "/v1/LoanFacility/";
    private static final Logger logger = LogManager.getLogger(LoanFacilityService.class);

    public LoanFacility getLoanFacility(String uuid) throws LoanFacilityException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Content-Type", "text/html");
        try {
            HttpResponse<String> reply =
                    Unirest
                            .get( "http://"+url+":"+port+path+uuid)

                            .headers(headers)
                            .asString();
           // logger.info("implementationClass="                    + this.getClass().getName());
            if (reply.getStatus() == HttpStatus.NOT_FOUND)
            {
                throw new LoanFacilityNotFoundException("LoanFacility not found uuid: "+uuid);
            }
            return objectMapper.readValue(reply.getBody(), LoanFacility.class);

        } catch (Exception ex) {
            logger.error("implementationClass="
                    + this.getClass().getName()
                    + ex);
            throw new LoanFacilityException("Something wenbt wrong." + ex);
        }
    }

}
