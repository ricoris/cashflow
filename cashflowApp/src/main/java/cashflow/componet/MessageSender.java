package cashflow.componet;


import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component
public class MessageSender {

    @Value("${spring.data.messaging.exchange}")
    String exchange;
    @Value("${spring.data.messaging.routing-key}")
    String routingKey;
    private RabbitTemplate rabbitTemplate;

    public MessageSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void broadcast(String message) {
        this.rabbitTemplate.convertAndSend(exchange, routingKey, message);
    }

}

