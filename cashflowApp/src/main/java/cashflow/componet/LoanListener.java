package cashflow.componet;

import cashflow.domain.LoanFacility;
import cashflow.finance.LoanCalculator;
import cashflow.model.Payment;
import cashflow.repositories.FinancePaymentsRepository;
import cashflow.service.IFeeTypeService;
import cashflow.service.IProductService;
import com.rabbitmq.client.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

@Component
public class LoanListener {

    @Value("${service.redis.skip-save-to-db}")
    public boolean skipSaveToDb;
    @Autowired
    private IProductService productService;
    @Autowired
    private IFeeTypeService feeTypeService;
    private static final Logger logger = LogManager.getLogger(LoanListener.class);
    private final CountDownLatch latch = new CountDownLatch(1);
    private final FinancePaymentsRepository financePaymentsRepository;

    public LoanListener(FinancePaymentsRepository financePaymentsRepository) {
        this.financePaymentsRepository = financePaymentsRepository;
        this.financePaymentsRepository.initialize();
    }
    int processedLoans = 0;
    long startTime = System.currentTimeMillis();
    @RabbitListener(queues = "${spring.data.messaging.receive-queue}", ackMode = "MANUAL")
    public void consumeMessageFromQueue(LoanFacility loanFacility, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws IOException {
        if (processedLoans == 0)
            startTime = System.currentTimeMillis();

        try {

            var product = productService.getProduct(Integer.parseInt(loanFacility.getProductTypeId()));
            var getFee = feeTypeService.getFeeType(product.getFeeType());
            if (product == null || getFee == null) {
                channel.basicNack(tag, false, false);
                logger.error(String.format("Error: MessageConsumer loanFacility elapsed time ms: {%d}", (System.currentTimeMillis() - startTime)), loanFacility.toString());
                return;
            }
            var payments = LoanCalculator.calculateAmortizationSchedule(loanFacility.getCurrentBalance(), product.getInterestRate(), loanFacility.getTerm());

            payments = payments.stream().map(t ->
                    {
                        t.setFeePaid(LoanCalculator.feeForMonth(getFee, t.getMonth()));
                        return t;
                    }
            ).collect(Collectors.toList());
            long startTimeDb = System.currentTimeMillis();
            if (skipSaveToDb){}
            else{
                financePaymentsRepository.setFinancePayments(new Payment(loanFacility.getId(), payments));
            }

            //logger.info(String.format("MessageConsumer Database call elapsed time ms: {%d}", (System.currentTimeMillis() - startTimeDb)), loanFacility.getId());
            processedLoans++;
            if (processedLoans == 1000) {
                var timeend = System.currentTimeMillis();
                //logger.info(String.format("MessageConsumer Database call elapsed time last loan ms: {%d}", (timeend - startTimeDb)), loanFacility.getId());
                logger.info(String.format("MessageConsumer loanFacility elapsed time 1000 loans ms: {%d} - Loan: {%s} - payment_rows: {%s}", (timeend - startTime), loanFacility.getId(), payments.size()));
                processedLoans  = 0;
            }
            channel.basicAck(tag, false);

            latch.countDown();

        } catch (Exception ex) {
            logger.error("MessageConsumer loanFacility", ex);
            channel.basicNack(tag, false, false);

        }
    }

}
