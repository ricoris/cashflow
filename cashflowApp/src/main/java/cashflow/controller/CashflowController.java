package cashflow.controller;

import cashflow.exception.LoanFacilityNotFoundException;
import cashflow.finance.LoanCalculator;
import cashflow.model.Customer;
import cashflow.model.Payment;
import cashflow.repositories.CustomerRepository;
import cashflow.repositories.FinancePaymentsRepository;
import cashflow.service.IFeeTypeService;
import cashflow.service.ILoanFacilityService;
import cashflow.service.IProductService;
import cashflow.service.SendService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/cashflow/v1")
public class CashflowController {

    private static final Logger logger = LogManager.getLogger(CashflowController.class);

    @Autowired
    private SendService sendService;
    @Autowired    private IFeeTypeService feeTypeService;
    @Autowired    private ILoanFacilityService loanFacilityService;
    @Autowired    private IProductService productService;
    private final CustomerRepository customerRepository;
    private FinancePaymentsRepository financePaymentsRepository;

    public CashflowController(SendService sendService, CustomerRepository customerRepository, FinancePaymentsRepository financePaymentsRepository) {

        this.sendService = sendService;
        this.customerRepository = customerRepository;
        this.customerRepository.initialize();
        this.financePaymentsRepository = financePaymentsRepository;
        this.financePaymentsRepository.initialize();
    }

    @PostMapping("/customer/{customerName}")
    public  ResponseEntity<String>  customerWithLoan(@RequestBody Customer customer, @PathVariable String customerName) {

        logger.info("API Customer : {}", () -> customer);

        sendService.CustomerSend(customer, customerName);

        customer.getLoans().forEach( loan -> {
           var arrayLst =  LoanCalculator.calculateAmortizationSchedule(loan.getLoadAmount().doubleValue(),loan.getAnnualInterestRate().doubleValue(),loan.getNumYears());
            var payment = new Payment(loan.getId(),arrayLst);

            financePaymentsRepository.setFinancePayments(payment);
        });

        return new ResponseEntity<>(
                "OK", HttpStatus.OK);

    }
    @GetMapping("/customer/{customerNum}")
    public ResponseEntity<String> getCustomer(@PathVariable String customerNum)
    {
        StringBuilder sb = new StringBuilder();

        var cust = customerRepository.getCustomer(customerNum);
        if (cust == null) {
            StringBuilder append = sb.append("No customers:");
            return new ResponseEntity<>(
                    sb.toString(), HttpStatus.OK);
        }
        StringBuilder append = sb.append("Customer number :")
                .append(cust.getCustomerNum()).append("\n")
                .append("Customer :")
                .append(cust.getCustomerName())
                .append("\n")
                .append("----------------------------------------------------\n");

        cust.getLoans().forEach(loan ->  sb.append("loadid " +loan.toString()+"\n"));

        cust.getLoans().forEach(loan -> {
            sb.append("----------------------------------------------------\n");
            sb.append("LoanId to look up "+loan.getId());
            sb.append(String.format("\n%8s%10s%10s%12s\n",
                    "Payment#", "Interest", "Principal", "Balance"));
            sb.append(String.format("%8s%10s%10s%12s\n\n",
                    "", "paid", "paid", ""));

            Payment payments = null;
            try {
                payments = financePaymentsRepository.getFinancePayments(loan.getId());
            } catch (LoanFacilityNotFoundException e) {

                logger.error("Error in getCustomer from CashflowControler ex: "+e);
            }
            payments.getFinancePayments().forEach(pay -> sb.append(LoanCalculator.stringScheduleItem(pay)+"\n"));
        });
        return new ResponseEntity<>(
                sb.toString(), HttpStatus.OK);

    }
}

