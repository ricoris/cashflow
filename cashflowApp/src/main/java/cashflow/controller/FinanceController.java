package cashflow.controller;

import cashflow.exception.*;
import cashflow.finance.LoanCalculator;
import cashflow.model.Payment;
import cashflow.repositories.FinancePaymentsRepository;
import cashflow.service.IFeeTypeService;
import cashflow.service.ILoanFacilityService;
import cashflow.service.IProductService;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Optionals;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/cashflow/v1")
public class FinanceController {

    private static final Logger logger = LogManager.getLogger(FinanceController.class);

    Gson gson = new Gson();
    @Autowired    private IFeeTypeService feeTypeService;
    @Autowired    private ILoanFacilityService loanFacilityService;
    @Autowired    private IProductService productService;

    private FinancePaymentsRepository financePaymentsRepository;

    public FinanceController( FinancePaymentsRepository financePaymentsRepository) {


        this.financePaymentsRepository = financePaymentsRepository;
        this.financePaymentsRepository.initialize();
    }
    @GetMapping("/loan")
    public ResponseEntity<String> get() throws LoanFacilityNotFoundException {
        StringBuilder sb = new StringBuilder();

        var payments = financePaymentsRepository.countFinancePayments();
        if (payments != null)
            sb.append("There is FinancePayments :"+payments);
        else
            sb.append("No data in database");

        return new ResponseEntity<>(gson.toJson(sb.toString()),HttpStatus.OK);
    }
    @GetMapping("/loan/{loanId}")
    public ResponseEntity<String> getCustomer(@PathVariable String loanId) throws LoanFacilityNotFoundException {
        StringBuilder sb = new StringBuilder();

         var payments = financePaymentsRepository.getFinancePayments(loanId);
         if (payments != null)
         payments.getFinancePayments().forEach(pay -> sb.append(LoanCalculator.stringScheduleItem(pay)+"\n"));
         else
             sb.append("No data");

        return new ResponseEntity<>(sb.toString(),HttpStatus.OK);
    }
    @GetMapping("/loanRest/{loanId}")
    public ResponseEntity<String> getCustomerRest(@PathVariable String loanId) throws LoanFacilityNotFoundException {
            return new ResponseEntity<>(
                    gson.toJson(
                            financePaymentsRepository.getFinancePayments(loanId)
                    ), HttpStatus.OK);

    }
    @GetMapping("/loanRecalc/{loanId}")
    public ResponseEntity<String> recalcCustomerRest(@PathVariable String loanId) throws JsonProcessingException {
        var loanFacility = loanFacilityService.getLoanFacility(loanId);
        if (loanFacility == null)
        {
            throw new LoanFacilityNotFoundException("Loan not found");
        }
        var product = productService.getProduct(Integer.parseInt(loanFacility.getProductTypeId()));
        var getFee = feeTypeService.getFeeType(product.getFeeType());

        var payments = LoanCalculator.calculateAmortizationSchedule(loanFacility.getCurrentBalance(), product.getInterestRate(), loanFacility.getTerm());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);

        payments = payments.stream().map(t ->
                {
                    t.setFeePaid(LoanCalculator.feeForMonth(getFee, t.getMonth()));
                    return t;
                }
        ).collect(Collectors.toList());

        financePaymentsRepository.setFinancePayments(new Payment(loanFacility.getId(), payments));
        return new ResponseEntity<>(
                objectMapper.writeValueAsString (
                        financePaymentsRepository.getFinancePayments(loanId)
                ), HttpStatus.OK);

    }


}

