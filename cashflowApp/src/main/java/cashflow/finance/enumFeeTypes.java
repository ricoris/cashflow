package cashflow.finance;

public enum enumFeeTypes {
    Monthly(12),
    Quarterly(4),
    SemiAnually(2),
    Anually(1),
    Once(0);
    private final int feeLevel;

    enumFeeTypes(int feeLevel) {
        this.feeLevel = feeLevel;
    }

    public int getFeeLevel() {
        return feeLevel;
    }
}
