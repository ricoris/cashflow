package cashflow.finance;

import cashflow.domain.FeeType;
import cashflow.model.FinancePaymentRows;

import java.util.ArrayList;
import java.util.List;

public class LoanCalculator {
    /**
     * https://stackoverflow.com/questions/28834825/java-loan-amortization
     * Calculate amortization schedule for all months.
     * @param principal - the total amount of the loan
     * @param annualInterestRate in percent
     * @param numMonths
     */
    public static List<FinancePaymentRows> calculateAmortizationSchedule(double principal, double annualInterestRate,
                                                                         int numMonths) {
        double interestPaid, principalPaid, newBalance;
        double monthlyInterestRate, monthlyPayment;
        int month;


        // Output monthly payment and total payment
        monthlyInterestRate = annualInterestRate / 12;
        monthlyPayment      = monthlyPayment(principal, monthlyInterestRate, numMonths);
        //System.out.format("Monthly Payment: %8.2f%n", monthlyPayment);
        //System.out.format("Total Payment:   %8.2f%n", monthlyPayment * numYears * 12);

        // Print the table header
        //printTableHeader();
        List<FinancePaymentRows> listPayments = new ArrayList<>();
        for (month = 1; month <= numMonths; month++) {
            // Compute amount paid and new balance for each payment period
            interestPaid  = principal      * (monthlyInterestRate / 100);
            principalPaid = monthlyPayment - interestPaid;
            newBalance    = principal      - principalPaid;

            // Output the data item
            var d = new FinancePaymentRows(month, interestPaid, principalPaid, newBalance,0);
            listPayments.add(d);
           // printScheduleItem(d);

            // Update the balance
            principal = newBalance;
        }
        return listPayments;
    }
    /**
     * Add fees to cashFlow
     *
     */
    public static double feeForMonth(FeeType fee, Integer month)
    {
        enumFeeTypes ft = enumFeeTypes.valueOf(fee.getFrequency());

        double feeCalculation = 0;
        switch (ft)
        {
            case Once:
                if (month == 1)
                    feeCalculation = fee.getAmount();
                break;
            case Anually:
                if (month % 12 == 0)
                {
                    feeCalculation = fee.getAmount();
                }
                break;
            case SemiAnually:
                if (month % 2 == 0)
                {
                    feeCalculation = fee.getAmount();
                }
                break;
            case Quarterly:
                if (month % 4== 0)
                {
                    feeCalculation = fee.getAmount();
                }
                break;
            case Monthly:
                feeCalculation = fee.getAmount();
                break;

        }
        return feeCalculation;
    }
    /**
     * @param loanAmount
     * @param monthlyInterestRate in percent
     * @param numMonths
     * @return the amount of the monthly payment of the loan
     */
    static double monthlyPayment(double loanAmount, double monthlyInterestRate, int numMonths) {
        monthlyInterestRate /= 100;  // e.g. 5% => 0.05
        return loanAmount * monthlyInterestRate /
                ( 1 - 1 / Math.pow(1 + monthlyInterestRate, numMonths) );
    }

    /**
     * Prints a table data of the amortization schedule as a table row.
     */
    private static void printScheduleItem(FinancePaymentRows pay) {
        System.out.format("%8d%10.2f%10.2f%12.2f\n",
                pay.getMonth(), pay.getInterestPaid(), pay.getPrincipalPaid(), pay.getNewBalance());
    }
    public static String  stringScheduleItem(FinancePaymentRows pay) {
        return String.format("%8d%10.2f%10.2f%12.2f%12.2f",
                pay.getMonth(), pay.getInterestPaid(), pay.getPrincipalPaid(), pay.getNewBalance(),pay.getFeePaid());
    }

    /**
     * Prints the table header for the amortization schedule.
     */
    private static void printTableHeader() {
        System.out.println("\nAmortization schedule");
        for(int i = 0; i < 40; i++) {  // Draw a line
            System.out.print("-");
        }
        System.out.format("\n%8s%10s%10s%12s\n",
                "Payment#", "Interest", "Principal", "Balance");
        System.out.format("%8s%10s%10s%12s\n\n",
                "", "paid", "paid", "");
    }
}

