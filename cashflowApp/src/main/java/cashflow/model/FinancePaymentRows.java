package cashflow.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FinancePaymentRows {

    private Integer Month;
    private double InterestPaid;
    private double PrincipalPaid;
    private double NewBalance;
    private double feePaid;
}
