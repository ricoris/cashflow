package cashflow.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Loan {


    private String Id;
    private Integer LoanType;
    private String LoanName;
    private BigDecimal LoadAmount;
    private BigDecimal AnnualInterestRate;
    private Integer NumYears;

}
