package cashflow.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Customer {

    private String CustomerNum;
    private String CustomerName;
    private List<Loan> loans;
}
