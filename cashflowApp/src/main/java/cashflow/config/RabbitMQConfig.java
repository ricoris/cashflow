package cashflow.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {
    @Value("${spring.data.messaging.receive-queue}")
    public String queueName;

    @Value("${spring.data.messaging.exchange}")
    String exchange;

    @Value("${spring.data.messaging.routing-key}")
    private String routingkey;


    private static final boolean NON_DURABLE = false;
    private static final boolean DO_NOT_AUTO_DELETE = false;
/*
    @Bean
    DirectExchange deadLetterExchange() {
        return new DirectExchange(EXCHANGE_DLQ, NON_DURABLE,
                DO_NOT_AUTO_DELETE);
    }

    @Bean
    Queue deadLetterQueue() {
        return QueueBuilder.durable(QUEUE_DLQ).build();
    }
     @Bean
    Binding deadLetterBinding() {
        return BindingBuilder.bind(deadLetterQueue()).to(deadLetterExchange()).with(ROUTING_KEY_DLQ);
    }
    */

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(exchange);
    }
      @Bean
    public Binding binding() {
        return BindingBuilder.bind(queue()).to(exchange()).with(routingkey);
    }


    @Bean
    public Queue queue() {

        return QueueBuilder.durable(queueName)
               // .withArgument("x-dead-letter-exchange", EXCHANGE_DLQ)
               // .withArgument("x-dead-letter-routing-key", ROUTING_KEY_DLQ)
                .build();
    }


    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }


}
