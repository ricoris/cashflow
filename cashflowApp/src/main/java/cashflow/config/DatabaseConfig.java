package cashflow.config;

import cashflow.model.Customer;
import cashflow.model.Payment;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.text.SimpleDateFormat;
import java.util.Objects;

@org.springframework.context.annotation.Configuration
public class DatabaseConfig {
    private static final Logger logger = LoggerFactory.getLogger(DatabaseConfig.class);

    /*
    @Value("${service.redis.use-gzip}")
    public boolean useGzip;
*/


    @Autowired
    Environment environment;

    /*
    @Autowired
    GitProperties gitProperties;
     */

    /*
        RedisDB Configuration
     */
    @Bean
    @ConditionalOnProperty(prefix = "storage", name = "cashflow", havingValue = "redisStorage")
    public RedisConnectionFactory connectionFactory() {

        RedisStandaloneConfiguration redisConfiguration = new RedisStandaloneConfiguration();
        redisConfiguration.setHostName(Objects.requireNonNull(environment.getProperty("spring.redis.host")));
        redisConfiguration.setPort(Integer.parseInt(Objects.requireNonNull(environment.getProperty("spring.redis.port"))));

        return new LettuceConnectionFactory(redisConfiguration);
    }

    @Bean("customerTemplate")
    @ConditionalOnProperty(prefix = "storage", name = "cashflow", havingValue = "redisStorage")
    public RedisTemplate<String, Customer> customerTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, Customer> roomTemplate = new RedisTemplate<>();
        roomTemplate.setConnectionFactory(connectionFactory);

        // Construct the serializer
        //Turn on the default type
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        //Set date format

        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        Jackson2JsonRedisSerializer<? extends Customer> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Customer.class);
        jackson2JsonRedisSerializer.setObjectMapper(objectMapper);

        // Attach serializer to the template to avoid caching behaviour for key, value sets.
        roomTemplate.setKeySerializer(new StringRedisSerializer());
        roomTemplate.setValueSerializer(jackson2JsonRedisSerializer);

        // Attach serializer to the template to avoid caching behaviour for Hash sets.
        roomTemplate.setHashKeySerializer(new StringRedisSerializer());
        roomTemplate.setHashValueSerializer(jackson2JsonRedisSerializer);

        return roomTemplate;
    }
    @Bean("financeTemplate")
    @ConditionalOnProperty(prefix = "storage", name = "cashflow", havingValue = "redisStorage")
    public RedisTemplate<String, Payment> financeTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, Payment> financeTemplate = new RedisTemplate<>();
        financeTemplate.setConnectionFactory(connectionFactory);

        // Construct the serializer
        //Turn on the default type
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        //Set date format

        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

        Jackson2JsonRedisSerializer<? extends Payment> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Payment.class);
        jackson2JsonRedisSerializer.setObjectMapper(objectMapper);

        // Attach serializer to the template to avoid caching behaviour for key, value sets.
        financeTemplate.setKeySerializer(new StringRedisSerializer());
        financeTemplate.setValueSerializer(jackson2JsonRedisSerializer);

        // Attach serializer to the template to avoid caching behaviour for Hash sets.
        financeTemplate.setHashKeySerializer(new StringRedisSerializer());
        financeTemplate.setHashValueSerializer(jackson2JsonRedisSerializer);

        //Override
        /*
        if (useGzip) {
            logger.info("Using Gzip in persistence in redis");
            // Set a custom serializer that will compress/decompress data to/from redis
            RedisSerializerGzip serializerGzip = new RedisSerializerGzip();
            financeTemplate.setValueSerializer(serializerGzip);
            financeTemplate.setHashValueSerializer(serializerGzip);
        }*/



        return financeTemplate;
    }

    /*
        MongoDB Configuration
     */
    @Bean
    @ConditionalOnProperty(prefix = "storage", name = "cashflow", havingValue = "mongoStorage")
    public MongoClient mongo() {

        ConnectionString  connectionString = new ConnectionString("mongodb://" +
                environment.getProperty("spring.data.mongodb.username") +
               ":" +
               environment.getProperty("spring.data.mongodb.password") +
                "@" +
                environment.getProperty("spring.data.mongodb.host") +
                ":" +
                environment.getProperty("spring.data.mongodb.port") +
                "/" +
                environment.getProperty("spring.data.mongodb.database")  +
                "?authSource=" +
                environment.getProperty("spring.data.mongodb.authentication-database"));

        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .build();


        return MongoClients.create(mongoClientSettings);
    }

    @Bean
    @ConditionalOnProperty(prefix = "storage", name = "cashflow", havingValue = "mongoStorage")
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongo(), Objects.requireNonNull(environment.getProperty("spring.data.mongodb.database")));
    }

}
