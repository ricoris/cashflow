package cashflow.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.boot.actuate.info.MapInfoContributor;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
@org.springframework.context.annotation.Configuration
public class Configuration {
    @Autowired
    Environment environment;
    @Autowired
    BuildProperties buildProperties;
    @Bean
    InfoContributor getInfoContributor() {
        Map<String, Object> details = new HashMap<>();
        details.put("GitHub", "https://bitbucket.org/ricoris/cashflow");
        details.put("DockerHub", "https://hub.docker.com/repository/docker/ricoris/cashflow/");
        details.put("DockerHubTag", "");
        Map<String, Object> wrapper = new HashMap<>();
        wrapper.put("MSDO Cashflow project", details);

        Map<String, Object> serverDetails = new HashMap<>();
        try {
            serverDetails.put("name", InetAddress.getLocalHost().getHostName());
            serverDetails.put("address", InetAddress.getLocalHost().getHostAddress());
            serverDetails.put("port", environment.getProperty("server.port"));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        wrapper.put("server", serverDetails);
        Map<String, Object> buildDetails = new HashMap<>();
        buildDetails.put("App name POM: ",buildProperties.getName());
        buildDetails.put("Artifact version: ",buildProperties.getVersion());
        buildDetails.put("Build time: ",buildProperties.getTime());
        buildDetails.put("Group id POM: ",buildProperties.getGroup());
        buildDetails.put("Artifact id POM: ",buildProperties.getArtifact());

        wrapper.put("Build parameters",buildDetails);

        Map<String, Object> authorDetails = new HashMap<>();
        authorDetails.put("ave@bec.dk", "Anton Vestergaard");
        authorDetails.put("riq@bec.dk", "Rico Sørensen");
        authorDetails.put("phg@bec.dk", "Peter Højbjerg");
        wrapper.put("Authors", authorDetails);

        return new MapInfoContributor(wrapper);
    }



}
