package finance;

import cashflow.domain.FeeType;
import cashflow.finance.LoanCalculator;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class LoanCalculatorTest {

    @Test
    void testLoanCalculations()
    {
        int monthsinYear = 12;
        double presentValue = 210000;
        double rateprePeriod = 0.05/12; // 5% pa
        int numberofperiods = 3*monthsinYear;
        var data = LoanCalculator.calculateAmortizationSchedule(presentValue,5,36);

        double  rate = presentValue * (rateprePeriod/(1-Math.pow((1+rateprePeriod),(numberofperiods*-1))));
        var testRAte = Math.round(rate*100)/100d;
        var calcRate = Math.round((data.get(0).getInterestPaid()+data.get(0).getPrincipalPaid())*100)/100d;
        assertThat(calcRate, is(testRAte));
        assertThat(data.size(),is(36));

    }
    @Test
    void testFeeDistributionAnually()
    {
        double presentValue = 100000;
        var fee = new FeeType();
        fee.setAmount(200);
        fee.setDescription("TEst fee");
        fee.setFrequency("Anually");
        var testFeeLaodn = LoanCalculator.feeForMonth(fee,12);
        assertThat(testFeeLaodn,is(200d));
        testFeeLaodn = LoanCalculator.feeForMonth(fee,1);
        assertThat(testFeeLaodn,is(0d));
    }
    @Test
    void testFeeDistributionOnce()
    {
        double presentValue = 100000;
        var fee = new FeeType();
        fee.setAmount(299);
        fee.setDescription("TEst fee");
        fee.setFrequency("Once");
        var testFeeLaodn = LoanCalculator.feeForMonth(fee,12);
        assertThat(testFeeLaodn,is(0d));
        testFeeLaodn = LoanCalculator.feeForMonth(fee,1);
        assertThat(testFeeLaodn,is(299d));
    }
    @Test
    void testFeeDistributionSemiAnually()
    {
        double presentValue = 100000;
        var fee = new FeeType();
        fee.setAmount(500);
        fee.setDescription("TEst fee");
        fee.setFrequency("SemiAnually");
        var testFeeLaodn = LoanCalculator.feeForMonth(fee,12);
        assertThat(testFeeLaodn,is(500d));
        testFeeLaodn = LoanCalculator.feeForMonth(fee,1);
        assertThat(testFeeLaodn,is(0d));
    }
}
