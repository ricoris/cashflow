package cashflow.repository;

import org.junit.jupiter.migrationsupport.rules.EnableRuleMigrationSupport;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
@Testcontainers
@EnableRuleMigrationSupport  //Making @Rule work with junit5
public class TestContainerMongoDBTest {

        private static final int MONGO_PORT = 27017;
    @Container
    static GenericContainer mongo  = new GenericContainer<>("mongo")
            .withExposedPorts(MONGO_PORT)
            .withEnv("MONGO_INITDB_ROOT_USERNAME", "lmadmin")
            .withEnv("MONGO_INITDB_ROOT_PASSWORD", "CamelsWithFeelings")
            .withReuse(true);
        public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

            @Override
            public void initialize(ConfigurableApplicationContext context) {
                // Start container
                mongo.start();

                // Override configuration
              //  String mongoIP = "spring.data.MongoDBCustomerTest.host=" + mongo.getContainerIpAddress();
              //  String mongoPort = "spring.data.MongoDBCustomerTest.port=" + mongo.getMappedPort(MONGO_PORT); // <- This is how you get the random port.
         /*      String pwd = "spring.data.mongodb.password= \"CamelsWithFeelings\"";
                String user = "spring.data.mongodb.username= \"lmadmin\"";
                String admin = "spring.data.mongodb.username= \"admin\"";*/
                String port = "spring.data.mongodb.port= "+ mongo.getMappedPort(MONGO_PORT);
                String host = "spring.data.mongodb.host= "+  mongo.getContainerIpAddress();
                TestPropertySourceUtils.addInlinedPropertiesToEnvironment(context, port,host); // <- This is how you override the configuration in runtime.
            }
        }
    }

