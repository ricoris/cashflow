package cashflow.repository;

import cashflow.exception.LoanFacilityNotFoundException;
import cashflow.finance.LoanCalculator;
import cashflow.model.Payment;
import cashflow.repositories.FinancePaymentsRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.io.Console;
import java.util.ArrayList;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;


/**
 * TDD of RoomRepository interface and
 * driving the MemoryRepository implementation.
 *
 * @author Team Alpha, Aarhus University.
 */
class CashRepositoryTests {


    private FinancePaymentsRepository storage;

    void setup(FinancePaymentsRepository storage) {
        this.storage = storage;
        this.storage.initialize();
    }

    public void shouldTestDoNotFindFacility() {
        Exception exception = assertThrows(LoanFacilityNotFoundException.class, () -> {
            var customer = storage.getFinancePayments("0909");
        });

        String expectedMessage = "Not found id0909";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));



    }

    void shouldAddCustomersFinances() throws LoanFacilityNotFoundException {
        ObjectMapper objectMapper = new ObjectMapper();

        var data = LoanCalculator.calculateAmortizationSchedule(1000,10,12);


        assertThat(storage.setFinancePayments(new Payment("1",data)),is(true));

        var loan = storage.getFinancePayments("1");

        assertThat(loan.getFinancePayments().size(),
                is(12));

    }

    void shouldAddTreeLoanToFinances() throws LoanFacilityNotFoundException {

        var data = LoanCalculator.calculateAmortizationSchedule(1000,10,12);
        var data2 = LoanCalculator.calculateAmortizationSchedule(2000,20,24);
        var data3 = LoanCalculator.calculateAmortizationSchedule(3000,40,36);

        assertThat(storage.setFinancePayments(new Payment("1",data)),is(true));
        assertThat(storage.setFinancePayments(new Payment("2",data2)),is(true));
        assertThat(storage.setFinancePayments(new Payment("3",data3)),is(true));

        var loan = storage.getFinancePayments("1");
        assertThat(loan.getFinancePayments().size(),
                is(12));
        loan = storage.getFinancePayments("2");
        assertThat(loan.getFinancePayments().size(),
                is(24));
        loan = storage.getFinancePayments("3");
        assertThat(loan.getFinancePayments().size(),
                is(36));

        var s = storage.countFinancePayments();
        assertThat(s,is(3L));

    }
    void speedTestLoansToFinances() throws LoanFacilityNotFoundException {
        long startTime = System.currentTimeMillis();
        for(int i = 0; i< 20000;i++) {
            var data = LoanCalculator.calculateAmortizationSchedule(i*1000, 10, 12);
            storage.setFinancePayments(new Payment(""+i, data));

        }
        long end = (System.currentTimeMillis() - startTime);
        System.out.println("Test class - save loans in ms: "+ end);
        assertThat("Timespend",end, lessThan(45000L));
    }

    }
