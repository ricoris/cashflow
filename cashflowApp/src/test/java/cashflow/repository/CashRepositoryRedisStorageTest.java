package cashflow.repository;

import cashflow.config.DatabaseConfig;
import cashflow.exception.LoanFacilityNotFoundException;
import cashflow.repositories.RedisFinancePaymentsRepositoryImpl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

// Load the RedisDB test container defined in TestContainerRedisDB
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RedisFinancePaymentsRepositoryImpl.class,
        DatabaseConfig.class},
        properties = {"storage.cashflow=redisStorage"})
@ContextConfiguration(initializers = TestContainerRedisDB.Initializer.class)
public class CashRepositoryRedisStorageTest extends CashRepositoryTests {

    @Autowired
    RedisFinancePaymentsRepositoryImpl redisFinancePaymentsRepository;

    @BeforeEach
    public void setup() {
        super.setup(redisFinancePaymentsRepository);
    }


    @Test()
    public void shouldTestDoNotFindFacility() {
        super.shouldTestDoNotFindFacility();
    }

    @Test()
    public void shouldAddCustomersFinances() throws LoanFacilityNotFoundException {
       super.shouldAddCustomersFinances();
    }
    @Test()
    public void shouldAddTreeLoanToFinances() throws LoanFacilityNotFoundException {super.shouldAddTreeLoanToFinances();}
    @Test
    public void speedTestLoansToFinances() throws LoanFacilityNotFoundException {
            super.speedTestLoansToFinances();
    }

}