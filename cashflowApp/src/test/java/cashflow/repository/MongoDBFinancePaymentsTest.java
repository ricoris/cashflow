package cashflow.repository;

import cashflow.config.DatabaseConfig;
import cashflow.exception.LoanFacilityNotFoundException;
import cashflow.repositories.FinancePaymentsRepository;
import cashflow.repositories.MongoDbFinancePaymentsRepositoryImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {MongoDbFinancePaymentsRepositoryImpl.class,
        DatabaseConfig.class},
        properties = {"storage.cashflow=mongoStorage"})

@ContextConfiguration(initializers = TestContainerMongoDBTest.Initializer.class)
public class MongoDBFinancePaymentsTest extends CashRepositoryTests {

    ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    FinancePaymentsRepository financePaymentsRepository;
    @BeforeEach
    public void setup() {
        super.setup(financePaymentsRepository);
    }

    @Test()
    public void shouldTestDoNotFindFacility() {
        super.shouldTestDoNotFindFacility();
    }

    @Test()
    public void shouldAddCustomersFinances() throws LoanFacilityNotFoundException {
        super.shouldAddCustomersFinances();
    }
    @Test()
    public void shouldAddTreeLoanToFinances() throws LoanFacilityNotFoundException {super.shouldAddTreeLoanToFinances();}

    /* wont do seed test in mongo to much time
    @Test

    public void speedTestLoansToFinances() throws LoanFacilityNotFoundException {
        super.speedTestLoansToFinances();
    }*/

}
