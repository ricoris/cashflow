package cashflow.service;

import cashflow.exception.ProductException;
import cashflow.exception.ProductNotFoundException;
import cashflow.exception.ProductUnirestException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.migrationsupport.rules.EnableRuleMigrationSupport;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {},
        properties = {"storage.cashflow=mongoStorage"})
@Testcontainers
@EnableRuleMigrationSupport  //Making @Rule work with junit5
public class ProductServiceTest {

    @Qualifier("TestDoubleProductService")
    @Autowired
    private IProductService product;

    @Test
    void getProduct() throws ProductUnirestException, ProductNotFoundException, ProductException {
        var d =  product.getProduct(11);
        assertThat(d.getId(),is("11"));
    }
    @Test()
    public void testExceptionGetProduct() {
        Exception exception = assertThrows(ProductNotFoundException.class, () -> {
            var customer = product.getProduct(10);
        });

        String expectedMessage = "Product Not found ProductId: 10";
        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));



    }

}