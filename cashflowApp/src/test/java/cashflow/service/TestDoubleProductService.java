package cashflow.service;


import cashflow.domain.Product;
import cashflow.exception.ProductException;
import cashflow.exception.ProductNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import kong.unirest.HttpResponse;
import kong.unirest.HttpStatus;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import lombok.SneakyThrows;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service("TestDoubleProductService")
public class TestDoubleProductService implements IProductService {
    @Value("${service.loan.management-url}")
    public String url;
    @Value("${service.loan.management-port}")
    public String port;

    private String path = "/v1/product/";
    private static final Logger logger = LogManager.getLogger(TestDoubleProductService.class);

    @SneakyThrows
    public Product getProduct(int id)
    {
        var bananer = new Product(""+id,"Bananer",100,10,10,10);

        if (id == 10) {
            throw new ProductNotFoundException("Product Not found ProductId: "+id);
        }
        if (id == 99)
        {
            throw new ProductException("Exception");
        }
        return bananer;
    }

}
