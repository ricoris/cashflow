package cashflow.service;


import cashflow.domain.LoanFacility;
import cashflow.exception.LoanFacilityNotFoundException;
import cashflow.exception.ProductException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service("TestDoubleLoanFacilityService")
public class TestDoubleLoanFacilityService implements ILoanFacilityService {
    @Value("${service.loan.management-url}")
    public String url;
    @Value("${service.loan.management-port}")
    public String port;

    private String path = "/v1/LoanFacility/";
    private static final Logger logger = LogManager.getLogger(TestDoubleLoanFacilityService.class);

    public LoanFacility getLoanFacility(String uuid) throws LoanFacilityNotFoundException {


        if (uuid.equals("None"))
        {
            throw new LoanFacilityNotFoundException("LoanFacility not found uuid: "+uuid);
        }
        if (uuid.equals("Uni"))
        {
            throw new UnirestException("Unirest");
        }

        var loanFacility = new LoanFacility();
        loanFacility.setId(uuid);

        return loanFacility;


    }

}
