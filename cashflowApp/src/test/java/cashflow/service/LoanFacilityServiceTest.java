package cashflow.service;

import cashflow.exception.LoanFacilityNotFoundException;
import kong.unirest.UnirestException;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.migrationsupport.rules.EnableRuleMigrationSupport;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {},
        properties = {"storage.cashflow=mongoStorage"})
@EnableRuleMigrationSupport  //Making @Rule work with junit5
public class LoanFacilityServiceTest {
    @Qualifier("TestDoubleLoanFacilityService")
    @Autowired
    private ILoanFacilityService facilityService;

    @SneakyThrows
    @Test
    void getLoanFacility()
    {
        var d =  facilityService.getLoanFacility("uuid");
        assertThat(d.getId(),is("uuid"));
    }
    @Test()
    public void testExceptionGetLoanFacility() {
        Exception exception = assertThrows(LoanFacilityNotFoundException.class, () -> {
            var customer = facilityService.getLoanFacility("None");
        });

        String expectedMessage = "LoanFacility not found uuid: None";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, is(expectedMessage));



    }
    @Test()
    public void testExceptionUnirestGetLoanFacility() {
        Exception exception = assertThrows(UnirestException.class, () -> {
            var customer = facilityService.getLoanFacility("Uni");
        });

        String expectedMessage = "Unirest";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, is(expectedMessage));



    }

}