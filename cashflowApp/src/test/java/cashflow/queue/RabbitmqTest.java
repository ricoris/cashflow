package cashflow.queue;

import cashflow.componet.MessageSender;
import cashflow.config.DatabaseConfig;
import cashflow.repositories.MongoDbFinancePaymentsRepositoryImpl;
import lombok.val;
import org.awaitility.Awaitility;
import org.junit.Rule;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.migrationsupport.rules.EnableRuleMigrationSupport;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;

/**
 * Look here for source https://gaddings.io/testing-spring-boot-apps-with-rabbitmq-using-testcontainers/
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@Testcontainers
@EnableRuleMigrationSupport  //Making @Rule work with junit5
@ContextConfiguration(initializers = RabbitmqTest.Initializer.class)
public class RabbitmqTest {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Container
    public static GenericContainer rabbit = new GenericContainer("rabbitmq:3-management")
            .withExposedPorts(5672, 15672);

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }
    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();
    public void RabbitmqTest(){}
    @Autowired
    private MessageSender messageSender;
    @Test
    public void testBroadcast() {
        messageSender.broadcast("Broadcast Test");

        Awaitility.await().atMost(10, TimeUnit.SECONDS).until(isMessageConsumed(), is(true));
    }
    private Callable<Boolean> isMessageConsumed() {
        return () ->
        {
            String s = outputStreamCaptor.toString();
            return s.contains("Broadcast Test");
        };
    }

    public static class Initializer implements
            ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {


                val values = TestPropertyValues.of(
                        "spring.rabbitmq.host=" + rabbit.getContainerIpAddress(),
                        "spring.rabbitmq.port=" + rabbit.getMappedPort(5672),
                        "spring.rabbitmq.username=guest",
                        "spring.rabbitmq.password=guest"
                );
                values.applyTo(configurableApplicationContext);

        }
    }
}
